# ML_Project1


## Add your files: reminiding on how to use gitlab

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/aniatato/ml_project1.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/aniatato/ml_project1/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***


## Description of project
The main aim of this project is to study in more detail various regression methods,
including the Ordinary Least Squares (OLS) method and Ridge regression.
The methods are in turn combined with resampling techniques like the
bootstrap method and cross validation.
We will first study how to fit polynomials to a specific two-dimensional
function called Franke’s function. This is a function which has been widely
used when testing various interpolation and fitting algorithms. Furthermore,
after having established the model and the method, we will employ resamling
techniques such as cross-validation and/or bootstrap in order to perform a proper
assessment of our models. We will also study in detail the so-called Bias-Variance
trade off.

## Support
Go to https://github.com/CompPhysics/MLErasmus/blob/master/doc/Projects/2023/Project1/ for further info

## Authors of this part of the ML final projects
Marti Rovira and Ania Tato.

## Project status
Project in current development: first steps of the project being made
